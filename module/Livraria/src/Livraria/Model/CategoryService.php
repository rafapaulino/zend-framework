<?php

namespace Livraria\Model;


class CategoryService
{
    /*
     * @var CategoryTable
     */
    protected $categoryTable;

    public function __construct(CategoryTable $table)
    {
        $this->categoryTable = $table;
    }

    public function fetchAll()
    {
        $resultSet = $this->categoryTable->select();
        return $resultSet;
    }
}