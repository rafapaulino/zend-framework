<?php

namespace Livraria\Service;

use Doctrine\ORM\EntityManager;
use Livraria\Entity\Configurator;

class Users extends AbstractService
{

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = "Livraria\Entity\Users";
    }

    public function insert(array $data)
    {
        $data['status'] = "A";
        $data['createdAt'] = new \DateTime("now");
        $data['updateAt'] = new \DateTime("now");
        $data['passwordReminder'] = "Teste de insert";
        $data['email2'] = "rafa_corre".time()."@hotmail.com";
        $data['rememberToken'] = "O loco";
        $data['avatar'] = "avatar.jpg";
        $data['type'] = 1;

        $entity = new $this->entity($data);
        $type = $this->em->getReference("Livraria\Entity\UserType", $data['type']);
        $entity->setType($type);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;

    }

    public function update(array $data)
    {
        $entity = $this->em->getReference($this->entity, $data['id']);

        if (empty($data['password']))
            unset($data['password']);

        $entity = Configurator::configure($entity, $data);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }

}