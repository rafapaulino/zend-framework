<?php

namespace Livraria\Service;

use Doctrine\ORM\EntityManager;
use Livraria\Entity\Configurator;

class Books extends AbstractService
{

    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
        $this->entity = "Livraria\Entity\Books";
    }

    public function insert(array $data)
    {
        $entity = new $this->entity($data);

        $category = $this->em->getReference("Livraria\Entity\Categories", $data['category']);
        $entity->setCategory($category);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;

    }

    public function update(array $data)
    {
        $entity = $this->em->getReference($this->entity, $data['id']);
        $entity = Configurator::configure($entity,$data);

        $categories = $this->em->getReference("Livraria\Entity\Categories", $data['category']);
        $entity->setCategory($categories);

        $this->em->persist($entity);
        $this->em->flush();

        return $entity;
    }
}