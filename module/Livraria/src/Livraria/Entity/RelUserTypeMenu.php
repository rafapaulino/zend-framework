<?php

namespace Livraria\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RelUserTypeMenu
 *
 * @ORM\Table(name="rel_user_type_menu", indexes={@ORM\Index(name="rel_user_type_type", columns={"type"}), @ORM\Index(name="rel_user_type_menu", columns={"menu"})})
 * @ORM\Entity
 */
class RelUserTypeMenu
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \Livraria\Entity\Menu
     *
     * @ORM\ManyToOne(targetEntity="Livraria\Entity\Menu")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="menu", referencedColumnName="id")
     * })
     */
    private $menu;

    /**
     * @var \Livraria\Entity\UserType
     *
     * @ORM\ManyToOne(targetEntity="Livraria\Entity\UserType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return RelUserTypeMenu
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return Menu
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * @param Menu $menu
     * @return RelUserTypeMenu
     */
    public function setMenu($menu)
    {
        $this->menu = $menu;
        return $this;
    }

    /**
     * @return UserType
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param UserType $type
     * @return RelUserTypeMenu
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

}

