<?php

namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController,
    Zend\View\Model\ViewModel;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

use Admin\Form\Login as LoginForm;

class AuthController extends AbstractActionController
{

    public function indexAction()
    {
        //exemplo de utilizacao do firephp
        // conferir http://www.douglaspasqua.com/2013/03/09/instalando-zend-framework-2-com-zftool-firephp-e-doctrine/
        $firephp = $this->getServiceLocator()->get('Zend\Log\FirePhp');
        $firephp->info("info log");
        $firephp->warn("warn log");
        $firephp->crit("critical log");

        $form = new LoginForm;
        $error = false;

        $request = $this->getRequest();
        if ($request->isPost())
        {
            $form->setData($request->getPost());
            if ($form->isValid())
            {
                $data = $request->getPost()->toArray();

                $auth = new AuthenticationService;

                $sessionStorage = new SessionStorage("Admin");
                $auth->setStorage($sessionStorage);

                $authAdapter = $this->getServiceLocator()->get('Livraria\Auth\Adapter');
                $authAdapter->setUsername($data['email'])
                    ->setPassword($data['password']);

                $result = $auth->authenticate($authAdapter);

                if ($result->isValid())
                {
                    $sessionStorage->write($auth->getIdentity()['user'], null);
                    return $this->redirect()->toRoute("livraria-admin", array('controller' => 'categories'));
                }
                else
                {
                    $error = true;
                }
            }
        }

        return new ViewModel(array('form'=>$form,'error'=>$error));
    }

    public function logoutAction()
    {
        $auth = new AuthenticationService;
        $auth->setStorage(new SessionStorage('Admin'));
        $auth->clearIdentity();

        return $this->redirect()->toRoute('livraria-admin-auth');
    }

}