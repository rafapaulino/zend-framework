<?php

namespace Admin\Controller;

class CategoriesController extends CrudController
{

    public function __construct()
    {
        $this->entity = "Livraria\Entity\Categories";
        $this->form = "Admin\Form\Categories";
        $this->service = "Livraria\Service\Categories";
        $this->controller = "categories";
        $this->route = "livraria-admin";
    }

}