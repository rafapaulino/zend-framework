<?php

namespace Admin\Form;

use Zend\Form\Form;

class Categories extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('categories');

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new CategoriesFilter);

        $this->add(array(
            'name' =>'id',
            'attibutes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'name' => 'name',
            'options' => array(
                'type' => 'text',
                'label' => 'Name'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => 'Entre com o nome'
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Save',
                'class' => 'btn-success'
            )
        ));
    }
}