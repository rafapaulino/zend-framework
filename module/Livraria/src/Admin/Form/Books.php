<?php

namespace Admin\Form;

use Zend\Form\Form,
    Zend\Form\Element\Select;

class Books extends Form
{

    protected $categories;

    public function __construct($name = null, array $categories = null)
    {
        parent::__construct('books');
        $this->categories  = $categories;

        $this->setAttribute('method', 'post');
        $this->setInputFilter(new BooksFilter);

        $this->add(array(
            'name' => 'id',
            'attibutes' => array(
                'type' => 'hidden'
            )
        ));

        $this->add(array(
            'name' => 'name',
            'options' => array(
                'type' => 'text',
                'label' => 'Name'
            ),
            'attributes' => array(
                'id' => 'name',
                'placeholder' => 'Entre com o nome'
            )
        ));

        $category = new Select();
        $category->setLabel("Category")
            ->setName("category")
            ->setOptions(array('value_options' => $this->categories)
            );
        $this->add($category);

        $this->add(array(
            'name' => 'author',
            'options' => array(
                'type' => 'text',
                'label' => 'Author'
            ),
            'attributes' => array(
                'id' => 'author',
                'placeholder' => 'Entre com o autor'
            ),
        ));

        $this->add(array(
            'name' => 'isbn',
            'options' => array(
                'type' => 'text',
                'label' => 'ISBN'
            ),
            'attributes' => array(
                'id' => 'isbn',
                'placeholder' => 'Entre com o ISBN'
            ),
        ));

        $this->add(array(
            'name' => 'value',
            'options' => array(
                'type' => 'text',
                'label' => 'Value'
            ),
            'attributes' => array(
                'id' => 'value',
                'placeholder' => 'Entre com o Valor'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Zend\Form\Element\Submit',
            'attributes' => array(
                'value' => 'Save',
                'class' => 'btn-success'
            )
        ));
    }

}