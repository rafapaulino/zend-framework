<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Livraria;

use Livraria\Model\CategoryService;
use Livraria\Model\CategoryTable;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\ModuleManager\ModuleManager;
use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;
use Livraria\Service\Categories as CategoriesService;
use Livraria\Service\Books as BooksService;
use Livraria\Service\Users as UsersService;
use Admin\Form\Books as BooksFrm;
use Livraria\Auth\Adapter as AuthAdapter;


class Module
{
    public function onBootstrap(MvcEvent $e)
    {
        $e->getApplication()->getEventManager()->getSharedManager()->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e)
        {
            $controller = $e->getTarget();
            $controllerClass = get_class($controller);
            $moduleNamespace = substr($controllerClass, 0, strpos($controllerClass, '\\'));
            $config = $e->getApplication()->getServiceManager()->get('config');
            if (isset($config['module_layouts'][$moduleNamespace]))
            {
                $controller->layout($config['module_layouts'][$moduleNamespace]);
            }
        }, 98);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                    'Admin' => __DIR__ . '/src/' .'Admin',
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Livraria\Model\CategoryService' => function($service) {
                    $dbAdapter = $service->get('Zend\Db\Adapter\Adapter');
                    $categoryTable = new CategoryTable($dbAdapter);
                    $categoryService = new CategoryService($categoryTable);
                    return $categoryService;
                },
                'Livraria\Service\Categories' => function($service) {
                    return new CategoriesService($service->get('Doctrine\ORM\EntityManager'));
                },
                'Livraria\Service\Books' => function($service) {
                    return new BooksService($service->get('Doctrine\ORM\EntityManager'));
                },
                'Livraria\Service\Users' => function($service) {
                    return new UsersService($service->get('Doctrine\ORM\EntityManager'));
                },
                'Admin\Form\Books' => function($service) {
                    $em = $service->get('Doctrine\ORM\EntityManager');
                    $repository = $em->getRepository('Livraria\Entity\Categories');
                    $categories = $repository->fetchPairs();
                    return new BooksFrm(null, $categories);
                },
                'Livraria\Auth\Adapter' => function($service) {
                    return new AuthAdapter($service->get('Doctrine\ORM\EntityManager'));
                },
                'Zend\Log\FirePhp' => function($sm) {
                    $writer_firebug = new \Zend\Log\Writer\FirePhp();
                    $logger = new \Zend\Log\Logger();
                    $logger->addWriter($writer_firebug);
                    return $logger;
                }
            )
        );
    }

    public function init(ModuleManager $moduleManager)
    {
        $sharedEvents = $moduleManager->getEventManager()->getSharedManager();
        $sharedEvents->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e) {
            $auth = new AuthenticationService;
            $auth->setStorage(new SessionStorage("Admin"));

            $controller = $e->getTarget();
            $matchedRoute = $controller->getEvent()->getRouteMatch()->getMatchedRouteName();

            if (!$auth->hasIdentity() and ($matchedRoute == "livraria-admin" or $matchedRoute == "livraria-admin-interna"))
            {
                return $controller->redirect()->toRoute('livraria-admin-auth');
            }
        }, 99);
    }

    public function getViewHelperConfig()
    {
        return array(
            'invokables' => array(
                'UserIdentity' => 'Livraria\View\Helper\UserIdentity'
            )
        );
    }
}
