;
(function (window, document, $, undefined) {
    'use strict';

    var app = (function () {

        var $private = {};
        var $public = {};
        
        //configuracao do plugin alertify para add as classes do twitter bootstrap
        $public.alertifyInit = function () {      
            alertify.defaults.transition = "slide";
            alertify.defaults.theme.ok = "btn btn-primary";
            alertify.defaults.theme.cancel = "btn btn-danger";
            alertify.defaults.theme.input = "form-control";
        };
        
        /*
         * os formularios com o atributo class formCookieSave terão 
         * o seu conteúdo salvo automaticamente
         * O objetivo é salvar a informação para ser usada quando o botão voltar do
         * navegador for clicado, assim o form vem preenchido e os datatables
         * fazem a consulta de acordo com o conteúdo do formulario, assim facilita
         * a vida do usuario final
         */
        $public.formCookieSave = function () {
            
            if ($('.formCookieSave').length > 0) {
                //faco um loop nos formularios que terao o conteudo salvo
                $('.formCookieSave').each(function(){
                   var form = $(this);
                   //grava o conteudo do form com as configuracoes padrao
                   //https://github.com/BenGriffiths/jquery-save-as-you-type
                   form.sayt();
                });
                
                //resetar o formulario e apagar os cookies
                $('.btnReset').on('click',function(){
                    var $this = $(this);
                    var form = $this.parents('form:first');
                    //limpa todos os campos do form
                    form.formReset();
                    form.sayt({'erase': true}); 
                });
            }
        };

        //valida os formularios do sistema
        $public.formValidate = function () {
            if ($('form').length > 0) {
                //form validator 
                $("form.formValidator").each(function(){
                    $(this).validate();
                });
            }
        };

        //ativa os tooltips do sistema
        $public.tooltipInit = function () {
            $('body').tooltip({
                selector: '[data-toggle="tooltip"]'
            });
        };
        
        //seleciona todos os checkboxes
        $public.checkAll = function () {
            //selecionando todos os checkbox
            //funcao para selecionar todos os campos
            $('.checkall').on('click',function(){
              /*
               * pegando os campos que serao selecionados - eu indico isso atraves do atributo 
               * value do checkbox pai (que serve para selecionar).
               */
               var $this = $(this);
               var selecionar = $('.'+$this.val());
               //caso o checkbox esteja com o checked = true     
               if($this.is(':checked')){
                   //fazendo o loop pelos campos
                   $.each(selecionar,function(index,item){
                        $(item).attr('checked',true);
                        $(item).prop('checked', true); //prop foi colocado por causa do twitter bootstrap
                   });
               //caso o checkbox nao esteja com o checked = true     
               }else{
                   //fazendo o loop pelos campos
                   $.each(selecionar,function(index,item){
                         $(item).attr('checked',false);
                         $(item).prop('checked', false); //prop foi colocado por causa do twitter bootstrap
                   });
               }
            
            });
        };
        
        //faz o upload do arquivos
        $public.upload = function(){
            
            $('.upload').each(function(){
                var $this = $(this);
                var urlUpload = $this.data('url');
                var token = $this.data('token');
                
                $this.fileupload({
                    url: urlUpload,
                    dataType: 'json',
                    headers: { 'X-XSRF-TOKEN' : token },
                    done: function (e, data) {
                        $.each(data.result.files, function (index, file) {
                            $('<p/>').text(file.name).appendTo('#files');
                        });
                    },
                    progressall: function (e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                        );
                    }
                }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');

                });
        };
        
        return $public;
    })();

    // Global
    window.app = app;
    app.alertifyInit();
    app.formValidate();
    app.tooltipInit();
    app.checkAll();
    app.formCookieSave();
    app.upload();

})(window, document, jQuery);